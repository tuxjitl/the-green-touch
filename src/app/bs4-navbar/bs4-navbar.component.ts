import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
// import { AngularFireAuth } from '@angular/fire/auth';
// import * as firebase from 'firebase/app';
// import { Observable } from 'rxjs';

@Component({
  selector: 'bs4-navbar',
  templateUrl: './bs4-navbar.component.html',
  styleUrls: ['./bs4-navbar.component.css'],
})
export class Bs4NavbarComponent {

  // user$:Observable<firebase.default.User | null>;
  constructor(public auth: AuthService) {
    // afAuth.authState.subscribe((user)=> this.user = user);
    
  }

  logout() {
    this.auth.logout();
  }
}
