import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Bs4NavbarComponent } from './bs4-navbar.component';

describe('Bs4NavbarComponent', () => {
  let component: Bs4NavbarComponent;
  let fixture: ComponentFixture<Bs4NavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Bs4NavbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Bs4NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
