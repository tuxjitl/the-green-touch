import { AuthService } from './../services/auth.service';
import { Component } from '@angular/core'

@Component({
    selector: 'login',
	templateUrl: './login.component.html'
})
export class LoginComponent {

	user: firebase.default.User;
    constructor(
		public auth: AuthService
		
	) {}
	login() {
		this.auth.login();
	}
}
