import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth:AuthService, private router:Router) { }
  // canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
  canActivate(route:ActivatedRouteSnapshot,state:RouterStateSnapshot) {
    return this.auth.user$.pipe(map(user => {
      if(user) {return true;}//we have a user ==> redirect to login page

      this.router.navigate(['/login'],{queryParams: {returnUrl:state.url}});
      return false;
    }));
  }
}
