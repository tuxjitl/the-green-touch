import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  user$: Observable<firebase.default.User | null>;

  constructor(private afAuth: AngularFireAuth, private route: ActivatedRoute) {
    this.user$ = afAuth.authState;
  }

  login() {
    let returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/';
    localStorage.setItem('returnUrl',returnUrl);

    // this.afAuth.signInWithPopup(new firebase.default.auth.GoogleAuthProvider());
    this.afAuth.signInWithRedirect(
      new firebase.default.auth.GoogleAuthProvider()
    ); //also works
  }
  logout() {
    this.afAuth.signOut();
  }
}
