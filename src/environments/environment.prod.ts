export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyB6P53GmkeEEfFsYa_w8CPIiy4c3eCOC4U',
    authDomain: 'the-green-touch-5aa4c.firebaseapp.com',
    databaseURL:
      'https://the-green-touch-5aa4c-default-rtdb.europe-west1.firebasedatabase.app',
    projectId: 'the-green-touch-5aa4c',
    storageBucket: 'the-green-touch-5aa4c.appspot.com',
    messagingSenderId: '876379930567',
    appId: '1:876379930567:web:845162488597877bffa3e2',
  },
};
