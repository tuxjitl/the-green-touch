// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyB6P53GmkeEEfFsYa_w8CPIiy4c3eCOC4U',
    authDomain: 'the-green-touch-5aa4c.firebaseapp.com',
    databaseURL:
      'https://the-green-touch-5aa4c-default-rtdb.europe-west1.firebasedatabase.app',
    projectId: 'the-green-touch-5aa4c',
    storageBucket: 'the-green-touch-5aa4c.appspot.com',
    messagingSenderId: '876379930567',
    appId: '1:876379930567:web:845162488597877bffa3e2',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
